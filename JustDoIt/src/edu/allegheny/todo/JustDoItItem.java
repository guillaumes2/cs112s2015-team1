package edu.allegheny.todo;

import java.util.*;
import java.text.SimpleDateFormat;


public class JustDoItItem{

    private static Calendar startDate = new GregorianCalendar();
    private static Calendar endDate = new GregorianCalendar();

    SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");

    private char priority;
    private String dateStarted;
    private String dateCompleted;
    private ArrayList<String> contexts = new ArrayList<String>();
    private ArrayList<String> projects = new ArrayList<String>();
    private boolean completed = false;
    private String task;
    private int completionDays = -1;

    public JustDoItItem(){}

        public void setPriority(String pri){
            priority = pri.charAt(1);
        }
        public void setDateStarted(String sd){
            try{
                dateStarted = sd;
                Date dateStart = df.parse(sd);
                startDate.setTime(dateStart);
                if (completed == true){
                     daysToComplete();
                }
            }
            catch (java.text.ParseException e){
                System.out.println("Unable to parse date strings.");
            }
        }
        public void setDateCompleted(String dc){
            try{
                dateCompleted = dc;
                Date dateDone = df.parse(dc);
                endDate.setTime(dateDone);
            }
            catch (java.text.ParseException e){
                System.out.println("Unable to parse date strings.");
            }
        }
        public void setContext(String c){
            contexts.add(c);
        }
        public void setProjects(String proj){
            projects.add(proj);
        }
        public void taskComplete(){
            completed = true;
        }
        public void setTask(String t){
            task = t;
        }
        public char getPriority(){
            return priority;
        }
        public String getDateStarted(){
            return dateStarted;
        }
        public String getDateCompleted(){
            return dateCompleted;
        }
        public boolean getCompleted(){
            return completed;
        }
        public String getTask(){
            return task;
        }
        public ArrayList<String> getProjects(){
            return projects;
        }
        public void daysToComplete (){
            completionDays = ((int)(endDate.getTimeInMillis()) - (int)(startDate.getTimeInMillis())) /(1000 * 60 * 60 * 24);
        }
        public ArrayList<String> getContext(){
            return contexts;
        }
        public int contextNumber(){
            return contexts.size();
        }
        public int projectNumber(){
            return projects.size();
        }
        public int getDaysToComplete(){
            return completionDays;
        }
}
