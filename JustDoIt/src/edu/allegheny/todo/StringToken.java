package edu.allegheny.todo;

import java.util.StringTokenizer;


public class StringToken{

    public static void main(String[]args){
        JustDoItItem JustDoIt = new JustDoItItem();
        StringTokenizer st = new StringTokenizer("(CMS112) +groupProject @Alden");
        while(st.hasMoreTokens()==true){
            String currentToken = st.nextToken();
            //System.out.println(st.nextToken());
            if(currentToken.startsWith("+")){
                JustDoIt.setTask(currentToken);
            }else if (currentToken.startsWith("@")){
                JustDoIt.setContext(currentToken);
            }else if (currentToken.startsWith("(")){
                JustDoIt.setPriority(currentToken);
            }

        }
    }

}
