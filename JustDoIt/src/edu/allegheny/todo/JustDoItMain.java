package edu.allegheny.todo;

import java.io.IOException;
import java.io.FileNotFoundException;
import java.util.Scanner;
import java.util.Iterator;

public class JustDoItMain {

    public static void main(String[]args) throws IOException {
        System.out.println("Welcome to Just Do It!");
        System.out.printf("The list that helps you get it done!\n\n");
        System.out.println("Choose a command: ");
        displayOptions();

        /* lets provide capabilities to:
         * view the list(default view will be by priority then date due)
         * priority-search(by Letter-group then date)
         * by due date (by date, then priority
         * location-search
         * etc
         */

        //import scanner to take in user answer to welcome message
       // Scanner scan = new Scanner(System.in);
        Scanner scan = new Scanner(System.in);
        JustDoItList doIt = new JustDoItList();

        int answer = 0;

        while (answer != 7){


        answer = Integer.parseInt(scan.next());

        if(answer == 1) {
            System.out.println("Task List");
            doIt.printList();
        }

        else if(answer == 2) {
            System.out.println("What is the priority of the task you are searching for? ");
            String input = scan.next();
            char priority = input.charAt(0);
            doIt.findPriority(priority);
        }
        else if(answer == 3){
            doIt.projectSearch();
        }
        else if(answer == 4) {
            doIt.contextSearch();
         }
        else if(answer == 5) {
            System.out.println("List of task that are due");
            doIt.findDueTasks();
         }
         else if(answer == 6) {
            doIt.timeAnalysis();
         }
         else if(answer == 7) {
                System.out.println("Thank you for the update! Come back soon!!!");
                break;
         }
         else {
             System.out.println("Your command options are: ");
             displayOptions();
         }
        System.out.printf("\n-------------------------------------------\n\n");
        System.out.println("Choose command or enter '7' to exit (for list of possible commands, enter 0): ");


        }//while
    }//main

    public static void displayOptions(){
        System.out.println("1. View list");
        System.out.println("2. Priority Search");
        System.out.println("3. Project Search");
        System.out.println("4. Context Search");
        System.out.println("5. See What's Due");
        System.out.println("6. Completion Time Analysis");
        System.out.println("7. I did it!/Exit");

    }
}//class


