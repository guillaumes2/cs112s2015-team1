package edu.allegheny.todo;

import java.io.*;
import java.util.*;

public class JustDoItList{

    private ArrayList<JustDoItItem> JustDoIt = new ArrayList<JustDoItItem>();
    private static final String FILE = "todo.txt";


    public JustDoItList() throws IOException{
        Scanner scan = new Scanner(new FileInputStream(FILE));
        //StringTokenizer st = new StringTokenizer(iterator.next());

        while (scan.hasNext()){
            String str = scan.nextLine();
            JustDoItItem jdi = new JustDoItItem();
            JustDoIt.add(jdi);
            jdi.setTask(str);
            StringTokenizer st = new StringTokenizer(str);

            String currentToken = st.nextToken();
            if (currentToken.startsWith("("))
                jdi.setPriority(currentToken);
            else if (currentToken.startsWith("x")){
                {
                    jdi.taskComplete();
                    currentToken = st.nextToken();
                    if (currentToken.startsWith("2")){
                        jdi.setDateCompleted(currentToken);
                    }
                }
            }
            if(st.hasMoreTokens()){
                currentToken = st.nextToken();
                if (currentToken.startsWith("2")){
                    jdi.setDateStarted(currentToken);
                }
            }

            while (st.hasMoreTokens()) {
                currentToken = st.nextToken();
                if(currentToken.startsWith("+")){
                    jdi.setProjects(currentToken);
                }else if (currentToken.startsWith("@")){
                    jdi.setContext(currentToken);
                }//tokenizing if/else
            }//while loop to tokenize Strings
        }//while loop to scan tasks

        /*
         *Debugging Outputs
         */

        //for(int i = 0; i < JustDoIt.size(); i++){
        //JustDoItItem k = JustDoIt.get(i);
        //System.out.println("-------------------------------------");
        //System.out.printf("Task %d: %s\n", i+1, k.getTask());
        //System.out.println("Completed?: "+k.getCompleted());
        //System.out.println("Date Started: "+k.getDateStarted());
        //System.out.println("Date Completed: "+k.getDateCompleted());
        //System.out.printf("Task Priority: %C\n", k.getPriority());
        //System.out.println("Contexts: "+k.getContext());
        //System.out.println("Projects: "+k.getProjects());
        //System.out.println("Days to Complete: "+k.getDaysToComplete());

        //}

    }//JustDoItList()


    public void printList(){
        Iterator it = JustDoIt.iterator();
        while (it.hasNext()){
            JustDoItItem current = (JustDoItItem)it.next();
            System.out.println(current.getTask());
        }
    }



    public void findPriority(char input){
        Iterator it = JustDoIt.iterator();
        boolean none = true;
        while(it.hasNext()){
            JustDoItItem current = (JustDoItItem)(it.next());
            char priChar = current.getPriority();
            if (priChar == input){
                none = false;
                System.out.println(current.getTask());
            }

        }
        if (none){
            System.out.println("You have no tasks of that priority.");
        }

    }//findPriority();

    public void findDueTasks(){
        Iterator findDue = JustDoIt.iterator();
        boolean none = true;
        while(findDue.hasNext()){
            JustDoItItem due = (JustDoItItem)(findDue.next());
            boolean status = due.getCompleted();
            if (status == false){
                System.out.println(due.getTask());
            }
        }
    }

    public ArrayList<String> findContexts(){
        Iterator findContexts = JustDoIt.iterator();
        ArrayList<String> contexts = new ArrayList<String>();
        while (findContexts.hasNext()){
            JustDoItItem c = (JustDoItItem)(findContexts.next());
            ArrayList<String> itemp = c.getContext();
            Iterator currentp = itemp.iterator();
            while (currentp.hasNext()){
                String cont = (String)(currentp.next());
                boolean duplicate = false;
                for (int i = 0; i < contexts.size(); i++){
                    if (cont.equals(contexts.get(i))){
                        duplicate = true;
                    }
                }
                if (duplicate == false){
                    contexts.add(cont);
                }
            }
        }
        return contexts;

    }
    public void contextSearch(){
        ArrayList<String> contexts = findContexts();
        if (!contexts.isEmpty()){
            System.out.println("These are your current contexts:");
            for (int i = 0; i < contexts.size(); i++){
                System.out.printf("(%d) %s\n", i, contexts.get(i));
            }
            System.out.println("Enter a number for which contexts's tasks you'd like to view.");
            Scanner scan = new Scanner(System.in);

            int cInt = scan.nextInt();
            String selectedC = contexts.get(cInt);
            Iterator findC = JustDoIt.iterator();
            System.out.printf("\nThese are the tasks associated with context %s: \n", selectedC);
            while (findC.hasNext()){
                JustDoItItem temp = (JustDoItItem)(findC.next());
                ArrayList<String> b = temp.getContext();
                Iterator bIt = b.iterator();
                while (bIt.hasNext()){
                    String g = (String)(bIt.next());
                    if (g.equals(selectedC)){
                        System.out.println(temp.getTask());
                    }
                }
            }
        }
        else {
            System.out.println("You currently have no contexts listed");
        }
    }

    public ArrayList<String> findProjects(){
        Iterator findProjects = JustDoIt.iterator();
        ArrayList<String> projects = new ArrayList<String>();
        while (findProjects.hasNext()){
            JustDoItItem p = (JustDoItItem)(findProjects.next());
            ArrayList<String> itemp = p.getProjects();
            Iterator currentp = itemp.iterator();
            while (currentp.hasNext()){
                String proj = (String)(currentp.next());
                boolean duplicate = false;
                for (int i = 0; i < projects.size(); i++){
                    if (proj.equals(projects.get(i))){
                        duplicate = true;
                    }
                }
                if (duplicate == false){
                    projects.add(proj);
                }
            }
        }
        return projects;
    }


    public void projectSearch(){
        ArrayList<String> projects = findProjects();
        if (!projects.isEmpty()){
            System.out.println("These are your current projects:");
            for (int i = 0; i < projects.size(); i++){
                System.out.printf("(%d) %s\n", i, projects.get(i));
            }
            System.out.println("Enter a number for which project's tasks you'd like to view.");
            Scanner scan = new Scanner(System.in);

            int pInt = scan.nextInt();
            String selectedP = projects.get(pInt);
            Iterator findP = JustDoIt.iterator();
            System.out.printf("\nThese are the tasks associated with project %s: \n", selectedP);
            while (findP.hasNext()){
                JustDoItItem temp = (JustDoItItem)(findP.next());
                ArrayList<String> v = temp.getProjects();
                Iterator vIt = v.iterator();
                while (vIt.hasNext()){
                    String g = (String)(vIt.next());
                    if (g.equals(selectedP)){
                        System.out.println(temp.getTask());
                    }
                }
            }
        }
        else {
            System.out.println("You currently have no projects listed");
        }
    }

    public void timeAnalysis(){
        System.out.printf("Would you like to analyse time completion by\n(1) Context\n(2) Project\n(3) All Tasks\n");
        Scanner input = new Scanner(System.in);
        int type = input.nextInt();
        ArrayList<JustDoItItem> items = new ArrayList<JustDoItItem>();
        if (type == 1){
            ArrayList<String> contexts = findContexts();

            if (!contexts.isEmpty()){
                System.out.println("These are your current contexts:");
                for (int i = 0; i < contexts.size(); i++){
                    System.out.printf("(%d) %s\n", i, contexts.get(i));
                }
                System.out.println("Enter a number for which contexts's tasks you'd like to analyse.");
                Scanner scan = new Scanner(System.in);

                int cInt = scan.nextInt();
                String selectedC = contexts.get(cInt);
                Iterator findC = JustDoIt.iterator();
                while (findC.hasNext()){
                    JustDoItItem temp = (JustDoItItem)(findC.next());
                    ArrayList<String> b = temp.getContext();
                    Iterator bIt = b.iterator();
                    while (bIt.hasNext()){
                        String g = (String)(bIt.next());
                        if (g.equals(selectedC)){
                            items.add(temp);
                        }
                    }
                }
            }
            else {
                System.out.println("You currently have no contexts listed");
            }
        }
        if (type == 2){
            ArrayList<String> projects = findProjects();
            if (!projects.isEmpty()){
                System.out.println("These are your current projects:");
                for (int i = 0; i < projects.size(); i++){
                    System.out.printf("(%d) %s\n", i, projects.get(i));
                }
                System.out.println("Enter a number for which project's tasks you'd like to analyse.");
                Scanner scan = new Scanner(System.in);

                int pInt = scan.nextInt();
                String selectedP = projects.get(pInt);
                Iterator findP = JustDoIt.iterator();
                while (findP.hasNext()){
                    JustDoItItem temp = (JustDoItItem)(findP.next());
                    ArrayList<String> v = temp.getProjects();
                    Iterator vIt = v.iterator();
                    while (vIt.hasNext()){
                        String g = (String)(vIt.next());
                        if (g.equals(selectedP)){
                            items.add(temp);
                        }
                    }
                }
            }
            else {
                System.out.println("You currently have no projects listed");
            }
        }
        else {
            items = JustDoIt;
        }
        analyseItems(items);
    }//

    public void analyseItems(ArrayList<JustDoItItem> al){
        ArrayList<JustDoItItem> complete = new ArrayList<JustDoItItem>();
        ArrayList<JustDoItItem> incomplete = new ArrayList<JustDoItItem>();
        Iterator alIt = al.iterator();
        while(alIt.hasNext()){
            JustDoItItem currentItem = (JustDoItItem)(alIt.next());
            if (currentItem.getCompleted()){
                complete.add(currentItem);
            }
            else {
                incomplete.add(currentItem);
            }
        }
        System.out.println("These are the items you have yet to complete in the specified field: ");
        for (int i = 0; i < incomplete.size(); i ++){
            JustDoItItem b = incomplete.get(i);
            System.out.println(b.getTask());
        }
        System.out.printf("\n These are the completed items in the specified field:\n");
        for (int i = 0; i < complete.size(); i++){
            JustDoItItem g = complete.get(i);
            System.out.println((g.getTask()));
        }

        int counter = 0;
        int totalDays = 0;

        for (int i = 0; i < complete.size(); i++){
            JustDoItItem j = complete.get(i);
            if (j.getDaysToComplete() != -1){
                counter++;
                totalDays = totalDays + j.getDaysToComplete();
            }
        }
        if (counter == 0){
            System.out.println("The tasks of the specified field do not have the necessary information to further analyse completion time");
        }

        else {
            System.out.printf("\nThere are %d tasks with the necessary information to analyse completion time.\n", counter);
            System.out.printf("It took %d total days to completed the tasks in the specified field\n", totalDays);
            double average = totalDays/counter;
            System.out.printf("Each task took an average of %.1f days.\n", average);
        }
    }






}//class
